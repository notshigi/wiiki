# Immortalizing WikiWikiWeb

This project consists of three parts.

* Tools for converting the original page markup to markdown.
* A website in which this new markdown mirror is hosted.
* A work-in progress network graph The Wiki inspired by the idea of '[VisualTour](wiki.kluv.in/a/VisualTour)'.
  * Intent is to integrate this with the website, and provide a map of the distribution of the communities represented in The Wiki.

If you are interested in playing around, or helping out, please don't hesitate to contact me.