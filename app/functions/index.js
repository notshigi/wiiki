const
    functions = require('firebase-functions'),
    express = require('express'),
    exphbs = require('express-handlebars'),
    { Irmin, Key } = require('./vendor/irmin/irmin')
    md = require('markdown-it')({
        html: true
    }).use(require('markdown-it-deflist'));

const app = express()


app.engine('handlebars', exphbs())
app.set('view engine', 'handlebars')
app.use(express.json())
app.use(express.static('../public'))

exports.app = functions.https.onRequest(app)

const db = new Irmin('http://localhost:8080/graphql')
const auth = new Irmin('http://localhost:8081/graphql').master()

const first = (x) => x[0]
const last = (x) => x[x.length - 1]

app.post('/set', async (req, res) => {
    const b = db.branch(req.body.from)

    const permitted = await auth.get(`w/${req.body.from}`)
    if (permitted === secret) {
        await b.set(
            `w/${req.body.title}`,
            JSON.stringify({
                title: req.body.title,
                text: req.body.content
            }))
        res.redirect(`/${req.body.from}/${req.body.title}`)
    }
})


app.post('/new', async (req, res) => {
    const b = db.branch(req.body.to)
    const permitted = await auth.get(`w/${req.body.to}`)

    if (permitted === null) {
        await auth.set(
            `w/${req.body.to}`,
            req.body.secret)
    }

    if (permitted === req.body.secret || permitted === null) {
        await b.set(
            `w/${req.body.title}`,
            JSON.stringify({
                title: req.body.title,
                text: req.body.content
            }))
        res.redirect(`/${req.body.to}/${req.body.title}`)
    }

})

app.post('/fork', async (req, res) => {
    const b = db.branch(req.body.to)
    const permitted = await auth.get(`w/${req.body.to}`)

    if (req.body.from !== req.body.to) {
        if (permitted === null) {
            await b.mergeWithBranch(req.body.from)
            await auth.set(
                `w/${req.body.to}`,
                req.body.secret)
            
            res.redirect(`/${req.body.to}/${req.body.title}`)
        }
    }
})

const getDerivatives = async (from, key) => {
    const hash = (await db.branch(from).lastModified(`w/${key}`, number=1)).hash[0]
    const derivatives = []
    for (branch of await db.branches()) {
        let r = await db.branch(branch).lastModified(`w/${key}`, number=10)
        if (r.hash.reverse()[0].hash !== hash.hash && r.hash.some(x => x.hash === hash.hash)) {
            derivatives.push({
                name: branch,
                url: `/${branch}/${key}`})
        }
    }
    return derivatives
}

app.get('/', async (_, res) => {    
    let r = await db.branch("epoch").get(`/w/WelcomeVisitors`)
    let article = JSON.parse(r)
    res.locals.meta = {
        title: 'wiiki - Welcome Visitors',
        description: 'An open-source mirror of WikiWikiWeb using markdown'
    }
    
    res.render('article', {
        content: md.render(article.text),
        markup: article.text,
        title: article.title,
        date: article.last_modified,
        derivatives: await getDerivatives("epoch", "WelcomeVisitors")
    })
})

app.get('/*', async (req, res) => {
    const rawPath = req.params[0]
    const path = rawPath.split('/')
    let r = await db.branch(first(path)).get(`/w/${last(path)}`)
    let article = JSON.parse(r)
    if (r && article.text) {
        res.locals.meta = {
            title: `wiiki - ${path}`,
        }

        res.render('article', {
            content: md.render(article.text),
            markup: article.text,
            title: splitCamelCase(rawPath),
            rawTitle: rawPath,
            date: article.date,
            derivatives: await getDerivatives(first(path), last(path), r.hash)
        })
    }
    else if (r && (! article.text)) {      
        res.locals.meta = { title: `wiiki - dead article :(` }
        res.render('missing', {
            title: `${req.params.title} is unavailable`
        })
    }

    else {
        res.locals.meta = { title: `wiiki - 404` }
        res.render('404', {
            title: `404: ${req.params.title} appears to be missing`
        })
    }
})

function splitCamelCase(s) {
    let r = /([a-z])([A-Z]+)/g
    return s.replace(r, (m, p1, p2) => `${p1} ${p2}`)
}

const port = 5420
app.listen(port, () => console.log(`Example app listening on port ${port}!`))
