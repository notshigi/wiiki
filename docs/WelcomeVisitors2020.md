**PS:** To read about changes to this version, see [ChangesInTwentyTwenty](ChangesInTwentyTwenty)

----

Welcome to the [WikiWikiWeb](WikiWikiWeb), also known as \"Wiki\". A lot of people had their first wiki experience here. This community has been around since 1995 and consists of many people. We always accept newcomers with valuable contributions. If you haven't used a wiki before, be prepared for a bit of [CultureShock](CultureShock). The usefulness of Wiki is in the freedom, simplicity, and power it offers.

This site's primary focus is [PeopleProjectsAndPatterns](PeopleProjectsAndPatterns) in [SoftwareDevelopment](SoftwareDevelopment). However, it is more than just an [InformalHistoryOfProgrammingIdeas](InformalHistoryOfProgrammingIdeas). It started there, but the theme has created a culture and [DramaticIdentity](DramaticIdentity) all its own. All Wiki content is [WorkInProgress](WorkInProgress). Most of all, this is a forum where people share ideas! It changes as people come and go. Much of the information here is subjective. If you are looking for a dedicated reference site, try [WikiPedia](WikiPedia); [WikiIsNotWikipedia](WikiIsNotWikipedia)!

-  Browse via [StartingPoints](StartingPoints), or use the [FindPage](FindPage) search facility to find your way.
-  Bookmark [RecentChanges](RecentChanges) and watch how things change.
-  Please pay attention to the tone of articles. See [WelcomeToWikiPleaseBePolite](WelcomeToWikiPleaseBePolite).
-  If you have beginner questions, you can see [NewUserQuestions](NewUserQuestions).
-  When learning [TextFormattingRules](TextFormattingRules) to edit pages, **please** use the [WikiWikiSandbox](WikiWikiSandbox) for all your trial edits.
-  If you have any other questions, ask the [WikiHelpDesk](WikiHelpDesk), and be patient.
-  The [WikiEngines](WikiEngines) page provides a reference to [WikiImplementations](WikiImplementations).
-  You can also select one of the [RandomPages](RandomPages), so with some luck, you start on a good point.
-  People should know a little [WikiHistory](WikiHistory).

Please read widely on this Wiki before adding new wiki pages or editing an old one. This helps to reduce unnecessary clutter.

[WikiSquatting](WikiSquatting) (using Wiki as personal Web space), [WalledGardens](WalledGardens) (a series of self-contained pages within a larger wiki), [ChatMode](ChatMode) ([ThreadMode](ThreadMode) without cleanup), and especially [WikiSpam](WikiSpam) (commercial advertising) are all frowned upon. We have several related [SisterSites](SisterSites) - religious debates or similar material are better suited to [TheAdjunct](TheAdjunct); purely artistic or whimsical stuff goes to [GreenCheese](GreenCheese) (currently defunct).

If you like the wiki concept and want to use a wiki for your own purposes (such as discussing topics other than those mentioned above), please consider other [PublicWikiForums](PublicWikiForums), or look at the [RunningYourOwnWikiFaq](RunningYourOwnWikiFaq). There are many [WikiWikiClones](WikiWikiClones) and [WikiEngines](WikiEngines) available. You can get help on [ChoosingaWiki](ChoosingaWiki) if you are overwhelmed by the big list of options.

------------------------------------------------------------------------

[CategoryWikiHelp](CategoryWikiHelp)
