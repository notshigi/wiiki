This file mostly contains quotes I have found reading The Wiki. Hopefully will evolve into something, but the haven't been touched in years.

------------

* Before processing raw page text QuickiWiki extracts links, stores them in a list, and replaces each with a placeholder. Once text formatting is done, it restores the links, removing the placeholders. You can also use this technique to implement literal text (e.g., code blocks). (background: if a URL contains a WikiLink, then the WikiLink part of the URL will be consumed by later WikiLink substitutions)

* (the expression matches empty lines and lines containing only whitespace (we don't look for the paragraph as a whole because the formatting script parses the text line-by-line)) -- http://doc.cat-v.org/bell_labs/structural_regexps/?

* The whole damn page http://wiki.c2.com/?SixSingleQuotes
    * The TextFormattingRules include a simple rule for specifying a bold face font: enclose the characters to be enboldened within triple single quotes. This, combined with an implementation accident, the fact that the quoted string can be zero length, makes six single quote characters valid markup. Enboldening an empty string would seem to be pointless except that the invisible markup interferes with other rules and some authors have found this interference convenient. Had I thought of this case when I wrote the rules I would have disallowed it. Now I can only lump it with all the other unplanned things this community has discovered and chosen to exploit. -- WardCunningham

    * Where you put SixSingleQuotes in a WikiName makes a difference. Placing them after the first letter prevents the entire WikiCase word from becoming a WikiName. Unfortunately, it creates a mis-spelled word for the spell checker to complain about. Placing SixSingleQuotes between capitalized words splits the WikiCase word in two, each of which can itself be a WikiName. Here are some examples:

    * "WardsWikiName" has SixSingleQuotes after the first letter. The spell checker will complain about "ardsWikiName".

* What's all the crap with hypothesis.js?

* Client side drawing of HTML pages should harm SEO but it wikiwikiweb consistently ranks top?

* Invalid HTML in every possible way makes it impossible to parse the content or use screen readers
    * The SixSingleQuotes method also confuses page scanners as it creates an unwanted empty STRONG element in the output.

    * Issue with `<p>`

    * Indented paragraphs are actually a special case of definitions. For example, to define a gnu, we would use 

* Little focus on the semantics of HTML and too much emphasis on how browsers style elements. In speaking of a semantic web, this is hell.

* "Use italics sparingly when possible, as it's harder to read (especially at low resolution) than non-italics." It's called emphasis, and it's used to *emphasize text*, not make it italic. The italization is a side-effect.

* Do not include secure connection like https, only http (without the s), just strip the 's' -- from http://wiki.c2.com/?VideoLink

* When talking about a Category without referencing it (i.e. you don't want to be added as a noise page to the Category search). Of course you don't get a link to the Category either so be careful when you do this that you are getting what you want. 

* "So why be different? Because you can? I would think that the wiki community should be working towards a common markup rather than splintering into umpteen variations." Indeed, markdown is a thing now

* What hot-headed fool am I to think I can fix the original wiki?

    * How about everyone gets over it, learn to use wiki as it is, and quit trying to fix and improve everything. It's worked fine for many years and doesn't need fixing. Do you know how tiring it is watching every person come in here thinking they can fix it, or improve it, well guess what, you can't, if you could, this would be your wiki.