* **generate.js**: Generate files needed: Wikimarkup > Markdown
  * **wikiparser.js**: Wikimarkup > HTML
  * **transform.js**: HTML > Markdown
* **format.js**: Generate files needed
    * **synthesize.js**: Infer synthetic data from article text

This folder contains the source code used in the transformation process of the WikiWikiWeb markup.

# The Process

The conversion process looks like this `wikimarkup > wikiscript html* > pandoc* > markdown`

Where:
* 'wikimarkup' is the the markup found in http://c2.com/wiki/remodel/pages
* 'wikiscript html' is the html rendered by the front-end JS on http://c2.com/wiki
* 'pandoc' is the document converter [Pandoc](https://pandoc.org/)
* The (*) implies content was modified prior to further processing

Modifications were done using replacements and regexes.

After the markdown is generated, various data which is later used in Neo4j is inferred from it, and written to a new JSON + CSV.
