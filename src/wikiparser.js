
/**
 * Just because understand it don't mean it's magic, but I'm sure this is
 * Collects the output of wikiparser and performs a series of substitutions on it.
 * @param {string} text to process 
 * @param {Set} internalLinks WikiWords which we have a corresponding article for
 */
module.exports = function (text, internalLinks) {
    var code = ''
    var codes = []

    var lines = text.split('\n') 
    var expand = lines.map(render).join("\n")
    var wikiparserFinalOutput = (expand + complete(''))

    return fixOutput(wikiparserFinalOutput)

    function escape(text) {
        return text
            .replace(/&/g, ';AMP;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;')
            .replace(/"/g, ';QUOT;')
    }

    function emphasis(text) {
        return text
            .replace(/'''(.*?)'''/g, '<strong>$1<\/strong>')
            .replace(/''(.*?)''/g, '<em>$1<\/em>')
            .replace(/^-----*/, '<hr>')
    }

    function enter(key, replacement) {
        return function (match, p1, p2) {
            var depth = p1.length || 0
            var adjust = ''
            code = key
            if (code != '...') {
                while (codes.length > depth) {
                    adjust += "</" + codes.pop() + ">"
                }
                while (codes.length < depth) {
                    adjust += "<" + key + ">";
                    codes.push(key)
                }
                if (codes.length && codes[codes.length - 1] != key) {
                    adjust += "</" + codes[codes.length - 1] + "><" + key + ">"
                    codes[codes.length - 1] = key
                }
            }
            return adjust + replacement.replace(/{p1}/, p1).replace(/{p2}/, p2)
        }
    }

    function complete(text) {
        return enter("", "")("", "", "") + text
    }

    function bullets(text) {
        code = ''
        result = text
            .replace(/^\s*$/, enter('...', '<p></p>'))
            .replace(/^(\t+)\*/, enter('ul', '<li>'))
            .replace(/^(\*+)/, enter('ul', '<li>'))
            .replace(/^(\t+)\d+\.?/, enter('ol', '<li>'))
            .replace(/^(\t+) :\t/, enter('blockquote', ''))
            .replace(/^(\t+)(.+):\t/, enter('dl', '<dt>{p2}<dd>'))
            .replace(/^(\s)/, enter('pre', '{p1}'))
        return result
    }

    function links(text, sanitize) {
        // link conversion happens in four phases:
        //   unexpected markers are adulterated
        //   links are found, converted, and stashed away properly escaped
        //   remaining text is processed and escaped
        //   unique markers are replaced with unstashed links
        var stashed = []

        function stash(text) {
            var here = stashed.length
            stashed.push(text)
            return "〖" + here + "〗"
        }

        function unstash(match, digits) {
            return stashed[+digits]
        }

        function internal(match, title, suffix) {
            if (internalLinks && internalLinks.has(title)) {
                if (suffix) {
                    return stash(`<a href=${title}>${title}</a>${suffix}`)
                }
                return stash(`<a href=${title}>${title}</a>`)
            }
            return title
        }

        function external(url) {
            if (url.match(/\.(gif|jpg|jpeg|png)$/)) {
                return stash(`<img src=${url}>`)
            } else {
                return stash(`<a href=${url} rel="nofollow">${url}</a>`)
            }
        }

        function isbn (match, isbn) {
            var code = isbn.replace(/[- ]/g, "")
            if (code.match(/^\d{9}.$/)) {
                return stash(`<a href=https://www.amazon.com/exec/obidos/ISBN=${code}>ISBN: ${isbn}</a> `)
            }
            else {
                return `ISBN: ${isbn}`
            }
        }

        var prepass = text
            .replace(/〖(\d+)〗/g, '〖 $1 〗')
            .replace(/\[?ISBN:? *([0-9- xX]{10,})\]?/i, isbn)
            .replace(/\b(https?|ftp|mailto|file|telnet|news):[^\s<>\[\]"'\(\)]*[^\s<>\[\]"'\(\)\,\.\?]/g, external)
            .replace(/\b([A-Z][a-z]+(?:[A-Z][a-z]+)+)(?:'{6,}(\w*))?\b/g, internal)
        var postpass = sanitize(prepass)
            .replace(/〖(\d+)〗/g, unstash)
        if (code == '') {
            postpass = complete(postpass)
        }
        return postpass
    }

    function inner(text) {
        text = escape(text)
        text = bullets(text)
        text = emphasis(text)
        return text
    }

    function render(text) {
        return links(text, inner)
    }

    function fixOutput(text) {
        return text
            // remove wikiword terminal sequences
            .replace(/\<strong\>\<\/strong\>/g, '')

            // pandoc fixes a bug coming from the wikiparser. but it has a side-effect, so we compensate 
            .replace(/(?<!<strong>)<em>(.*)<strong><\/em>(.*)<\/strong>/g, fixShuffledHtmlBug)
            .replace(/<strong><em>(.*)<\/strong><\/em>(.*)/g, fixShuffledTextBug)

            .replace(/<pre>([\s\S]*?)<\/pre>/g, fixFormattedTextInCodeBlockBug)
            .replace(/<pre>([\s\S]*?)<\/pre>/g, fixExtraneousIndentationBug)

            // Todo: Can cause problems as the regex may not be complicated enough.
            // Avoid markdown not triggering formatted input due to special characters:
            // \`*_{}[]()>#+-.!
            .replace(/<em>(.*)<\/em><strong>([\`*_{}\[\]()>#+-.!])(.+)([\`*_{}\[\]()>#+-.!])<\/strong><em>(.*)<\/em>/g, fixInbetweenHtmlBugForStrong)
            .replace(/<strong>(.*)<\/strong><em>([\`*_{}\[\]()>#+-.!])(.+)([\`*_{}\[\]()>#+-.!])<\/em><strong>(.*)<\/strong>/g, fixInbetweenHtmlBugForEm)

            // Upgrade outgoing links
            .replace('http://', 'https://')
    }

    function fixShuffledTextBug(match, p1, p2) {
        return `<strong><em>${p1}</em></strong>${p2}`
    }

    function fixShuffledHtmlBug(match, p1, p2) {
        return `<em>${p1}</em><strong>${p2}</strong>`
    }


    // <em>Italic</em><strong>[Bold]</strong><em>Italic</em> - from
    // <em>Italic</em>[<strong>Bold</strong>]<em>Italic</em> - to
    function fixInbetweenHtmlBugForEm(match, strongPre, prefix, italic, suffix, strongPost) {
        return `<strong>${strongPre}</strong>${prefix}<em>${italic}</em>${suffix}<strong>${strongPost}</strong>`
    }

    // <strong>Bold</strong><em>[Italic]</em><strong>Bold</strong> - from
    // <strong>Bold</strong>[<em>Italic</em>]<strong>Bold</strong> - to
    function fixInbetweenHtmlBugForStrong(match, italicPre, prefix, strong, suffix, italicPost) {
        return `<em>${italicPre}</em>${prefix}<strong>${strong}</strong>${suffix}<em>${italicPost}</em>`
    }

    function fixFormattedTextInCodeBlockBug(match, p1) {
        p1 = p1.replace(/<p><\/p>/g, '')
        return `<pre>${p1}</pre>`
    }

    // Wiki preserves leading spaces used to denote code blocks
    // We don't.
    function fixExtraneousIndentationBug(match, p1) {
        let lines = p1.split('\n')

        let leading = getLeadingSpaces(lines[0])
        let leadingRe = new RegExp('^' + leading)

        p1 = lines.map((x) => x.replace(leadingRe, '')).join('\n')
        return `<pre>${p1}</pre>`
    }

    function getLeadingSpaces(s) {
        let match = s.match(/^(\s)*(?=[\S])/)
        if (match) {
            return match[0]
        }
    }
}