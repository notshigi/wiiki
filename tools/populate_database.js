const fs = require('fs')
const path = require('path')

const admin = require('firebase-admin');

admin.initializeApp({
    credential: admin.credential.applicationDefault(),
    databaseURL: "https://wiiki-0.firebaseio.com"
});
let db = admin.firestore();

let articles = JSON.parse(fs.readFileSync(path.join(__dirname, '.compiled/articles.json'), encoding = "utf8", flag = 'r'))

async function main() {
    let batch = db.batch()
    
    for (let i = 0, len = articles.length; i < len; i++) {
        article = articles[i]
        
        if ((i+1) % 498 == 0) {
            console.log(`comitting batch from ${(i+1) - 498} to ${i+1}`)
            await batch.commit()
            await sleep(2000)
            batch = db.batch()
        }
        else {
            let docRef = db.collection('articles').doc(article.title)
            batch.set(docRef, {
                text: article.text || null,
                title: article.title || null,
                last_modified: article.date || null
            })
        }
    }
}
    
function sleep(millis) {
    return new Promise(resolve => setTimeout(resolve, millis))
}

main()